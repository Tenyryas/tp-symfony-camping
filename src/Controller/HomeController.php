<?php


namespace App\Controller;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class HomeController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="home")
     *
     * @return Response
     */
    public function bienvenue()
    {
        return $this->render("home.html.twig");
    }

    /**
     * @Route("/reservation", methods={"GET"}, name="reservation")
     *
     * @return Response
     */
    public function reserver()
    {
        // TODO: le template de réservation, redirection temporaire vers index en attendant

        return $this->redirectToRoute("home");
    }

    /**
     * @Route("/mon-compte", methods={"GET"}, name="mon-compte")
     *
     * @return Response
     */
    public function monCompte()
    {
        // TODO: le template du compte, redirection temporaire vers index en attendant

        return $this->redirectToRoute("home");
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastEmail = $authenticationUtils->getLastUsername();

        return $this->render("login.html.twig",
            [
                'last_email'=> $lastEmail,
                'error' => $error
            ]
        );
    }
}