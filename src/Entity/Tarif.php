<?php

namespace App\Entity;

use App\Repository\TarifRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TarifRepository::class)
 */
class Tarif
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=LocationLot::class, mappedBy="id_label")
     */
    private $locationLots;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    public function __construct()
    {
        $this->locationLots = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|LocationLot[]
     */
    public function getLocationLots(): Collection
    {
        return $this->locationLots;
    }

    public function addLocationLot(LocationLot $locationLot): self
    {
        if (!$this->locationLots->contains($locationLot)) {
            $this->locationLots[] = $locationLot;
            $locationLot->setIdLabel($this);
        }

        return $this;
    }

    public function removeLocationLot(LocationLot $locationLot): self
    {
        if ($this->locationLots->removeElement($locationLot)) {
            // set the owning side to null (unless already changed)
            if ($locationLot->getIdLabel() === $this) {
                $locationLot->setIdLabel(null);
            }
        }

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }
}
