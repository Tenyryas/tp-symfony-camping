<?php

namespace App\Entity;

use App\Repository\LocationLotRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LocationLotRepository::class)
 */
class LocationLot
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $lot_type;

    /**
     * @ORM\ManyToOne(targetEntity=Tarif::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_tarif;

    /**
     * @ORM\ManyToOne(targetEntity=LocationLabel::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_label;

    /**
     * @ORM\OneToMany(targetEntity=Reservation::class, mappedBy="id_loc", orphanRemoval=true)
     */
    private $id_reservations;

    /**
     * @ORM\ManyToOne(targetEntity=Auth::class, inversedBy="locationLots")
     */
    private $auth;

    public function __construct()
    {
        $this->id_reservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLotType(): ?int
    {
        return $this->lot_type;
    }

    public function setLotType(int $lot_type): self
    {
        $this->lot_type = $lot_type;

        /*
         * Types de terrains
         *
         * M-H 3 personnes: 1
         * M-H 4 personnes: 2
         * M-H 5 personnes: 3
         * M-H 6-8 personnes: 4
         * Caravane 2 places: 5
         * Caravane 4 places: 6
         * Caravane 6 places: 7
         * Emplacement 8m²: 8
         * Emplacement 12m²: 9
         */

        return $this;
    }

    public function getIdTarif(): ?Tarif
    {
        return $this->id_tarif;
    }

    public function setIdTarif(?Tarif $id_tarif): self
    {
        $this->id_tarif = $id_tarif;

        return $this;
    }

    public function getIdLabel(): ?LocationLabel
    {
        return $this->id_label;
    }

    public function setIdLabel(?LocationLabel $id_label): self
    {
        $this->id_label = $id_label;

        return $this;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getIdReservations(): Collection
    {
        return $this->id_reservations;
    }

    public function addIdReservation(Reservation $idReservation): self
    {
        if (!$this->id_reservations->contains($idReservation)) {
            $this->id_reservations[] = $idReservation;
            $idReservation->setIdLoc($this);
        }

        return $this;
    }

    public function removeIdReservation(Reservation $idReservation): self
    {
        if ($this->id_reservations->removeElement($idReservation)) {
            // set the owning side to null (unless already changed)
            if ($idReservation->getIdLoc() === $this) {
                $idReservation->setIdLoc(null);
            }
        }

        return $this;
    }

    public function getAuth(): ?Auth
    {
        return $this->auth;
    }

    public function setAuth(?Auth $auth): self
    {
        $this->auth = $auth;

        return $this;
    }
}
