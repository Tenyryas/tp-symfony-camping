<?php

namespace App\Entity;

use App\Repository\AuthRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=AuthRepository::class)
 * @method string getUserIdentifier()
 */
class Auth implements UserInterface, \Serializable, \Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="integer")
     */
    private $account_type;

    /**
     * @ORM\OneToMany(targetEntity=LocationLot::class, mappedBy="auth")
     */
    private $locationLots;

    /**
     * @ORM\Column(type="string", length=254)
     */
    private $email;

    public function __construct()
    {
        $this->locationLots = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUser(): ?User
    {
        return $this->id_user;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(User $id_user): self
    {
        $this->email = $id_user->getEmail();

        return $this;
    }

    public function setIdUser(User $id_user): self
    {
        $this->id_user = $id_user;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getAccountType(): ?int
    {
        return $this->account_type;
    }

    public function setAccountType(int $account_type): self
    {
        $this->account_type = $account_type;

        /*
         * type de comptes :
         * - Admin: 1
         * - Propriétaires: 2
         */

        return $this;
    }

    public function serialize()
    {
        return serialize([
            $this->id,
            $this->email,
            $this->password
        ]);
    }

    public function unserialize($serialized)
    {
        list($this->id, $this->email, $this->password) =
            unserialize($serialized, ["allowed_classes" => false]);
    }

    public function getRoles()
    {
        switch ($this->account_type) {
            case 1:
                return ['ROLE_ADMIN'];
            case 2:
                return ['ROLE_USER'];
        }
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getUsername(): string
    {
        return $this->getEmail();
    }

    public function __call($name, $arguments)
    {
        // TODO: Implement @method string getUserIdentifier()
    }

    /**
     * @return Collection|LocationLot[]
     */
    public function getLocationLots(): Collection
    {
        return $this->locationLots;
    }

    public function addLocationLot(LocationLot $locationLot): self
    {
        if (!$this->locationLots->contains($locationLot)) {
            $this->locationLots[] = $locationLot;
            $locationLot->setAuth($this);
        }

        return $this;
    }

    public function removeLocationLot(LocationLot $locationLot): self
    {
        if ($this->locationLots->removeElement($locationLot)) {
            // set the owning side to null (unless already changed)
            if ($locationLot->getAuth() === $this) {
                $locationLot->setAuth(null);
            }
        }

        return $this;
    }
}
