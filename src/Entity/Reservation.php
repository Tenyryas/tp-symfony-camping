<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReservationRepository::class)
 */
class Reservation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date_start;

    /**
     * @ORM\Column(type="date")
     */
    private $date_end;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbre_adulte;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbre_enfants;

    /**
     * @ORM\Column(type="integer")
     */
    private $pool_adultes;

    /**
     * @ORM\Column(type="integer")
     */
    private $pool_enfants;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="id_reservations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_user;

    /**
     * @ORM\ManyToOne(targetEntity=LocationLot::class, inversedBy="id_reservations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_loc;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateStart(): ?\DateTimeInterface
    {
        return $this->date_start;
    }

    public function setDateStart(\DateTimeInterface $date_start): self
    {
        $this->date_start = $date_start;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->date_end;
    }

    public function setDateEnd(\DateTimeInterface $date_end): self
    {
        $this->date_end = $date_end;

        return $this;
    }

    public function getNbreAdulte(): ?int
    {
        return $this->nbre_adulte;
    }

    public function setNbreAdulte(int $nbre_adulte): self
    {
        $this->nbre_adulte = $nbre_adulte;

        return $this;
    }

    public function getNbreEnfants(): ?int
    {
        return $this->nbre_enfants;
    }

    public function setNbreEnfants(int $nbre_enfants): self
    {
        $this->nbre_enfants = $nbre_enfants;

        return $this;
    }

    public function getPoolAdultes(): ?int
    {
        return $this->pool_adultes;
    }

    public function setPoolAdultes(int $pool_adultes): self
    {
        $this->pool_adultes = $pool_adultes;

        return $this;
    }

    public function getPoolEnfants(): ?int
    {
        return $this->pool_enfants;
    }

    public function setPoolEnfants(int $pool_enfants): self
    {
        $this->pool_enfants = $pool_enfants;

        return $this;
    }

    public function getIdUser(): ?User
    {
        return $this->id_user;
    }

    public function setIdUser(?User $id_user): self
    {
        $this->id_user = $id_user;

        return $this;
    }

    public function getIdLoc(): ?LocationLot
    {
        return $this->id_loc;
    }

    public function setIdLoc(?LocationLot $id_loc): self
    {
        $this->id_loc = $id_loc;

        return $this;
    }
}
