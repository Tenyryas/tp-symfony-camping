<?php

namespace App\Repository;

use App\Entity\LocationLot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LocationLot|null find($id, $lockMode = null, $lockVersion = null)
 * @method LocationLot|null findOneBy(array $criteria, array $orderBy = null)
 * @method LocationLot[]    findAll()
 * @method LocationLot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocationLotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LocationLot::class);
    }

    public function resetAutoIncrement() {
        $tableName = $this->getClassMetadata()->getTableName();
        $connection = $this->getEntityManager()->getConnection();
        $connection->executeStatement("ALTER TABLE " . $tableName . " AUTO_INCREMENT = 1;");
    }

    // /**
    //  * @return LocationLot[] Returns an array of LocationLot objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LocationLot
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
