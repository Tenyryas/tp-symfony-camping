<?php

namespace App\DataFixtures;

use App\Entity\Auth;
use App\Entity\LocationLabel;
use App\Entity\LocationLot;
use App\Entity\Reservation;
use App\Entity\Tarif;
use App\Entity\User;
use App\Repository\AuthRepository;
use App\Repository\LocationLabelRepository;
use App\Repository\LocationLotRepository;
use App\Repository\ReservationRepository;
use App\Repository\TarifRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordHasherInterface
     */
    private $encoder, $em, $auth, $user, $tarif, $labelLoc, $location, $reserv;

    public function __construct(
        UserPasswordHasherInterface $encoder,
        AuthRepository $auth,
        UserRepository $user,
        TarifRepository $tarif,
        LocationLabelRepository $labelLoc,
        LocationLotRepository $location,
        ReservationRepository $reserv,
        EntityManagerInterface $em
    )
    {
        $this->encoder = $encoder;
        $this->auth = $auth;
        $this->user = $user;
        $this->tarif = $tarif;
        $this->labelLoc = $labelLoc;
        $this->location = $location;
        $this->reserv = $reserv;
        $this->em = $em;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');


        $this->auth->resetAutoIncrement();
        $this->user->resetAutoIncrement();
        $this->tarif->resetAutoIncrement();
        $this->labelLoc->resetAutoIncrement();
        $this->location->resetAutoIncrement();
        $this->reserv->resetAutoIncrement();

        // Création compte admin
        $userAdmin = new User();
        $userAdmin->setNom('Admin')->setPrenom('Admin')
            ->setEmail('admin@admin.admin');
        $manager->persist($userAdmin);

        $AuthAdmin = new Auth();
        $AuthAdmin->setIdUser($userAdmin)
            ->setPassword($this->encoder->hashPassword($AuthAdmin, 'admin'))
            ->setAccountType(1)
            ->setEmail($userAdmin);
        $manager->persist($AuthAdmin);


        // Création Propriétaires
        for($i = 1; $i <= 30; $i++)
        {
            $userProprio = new User();
            $userProprio->setNom($faker->lastName)->setPrenom($faker->firstName())
                ->setEmail($faker->email);
            $manager->persist($userProprio);

            $AuthProprio = new Auth();
            $AuthProprio->setIdUser($userProprio)
                ->setPassword($this->encoder->hashPassword($AuthProprio, 'proprio'))
                ->setAccountType(2)
                ->setEmail($userProprio);
            $manager->persist($AuthProprio);
        }

        // Tarifs
        $tarifs = array(2000, 2400, 2700, 3400, 1500, 1800, 2400, 1200, 1400);
        foreach ($tarifs as $value){
            $bddTarifs = new Tarif();
            $bddTarifs->setPrice($value);
            $manager->persist($bddTarifs);
        }


        // Labels Locations
        $labelsLoc = array(
            "Mobile-home 3 personnes",
            "Mobile-home 4 personnes",
            "Mobile-home 5 personnes",
            "Mobile-home 6-8 personnes",
            "Caravane 2 places",
            "Caravane 4 places",
            "Caravane 6 places",
            "Emplacement 8m²",
            "Emplacement 12m²"
        );
        foreach ($labelsLoc as $label){
            $bddLabelsLoc = new LocationLabel();
            $bddLabelsLoc->setLabel($label);
            $manager->persist($bddLabelsLoc);
        }
        $manager->flush();
        
        // Mobile-homes proprios
        for( $i=1; $i <= 30; $i++ )
        {
            $location = new LocationLot();
            $lot_type = mt_rand(1, 4);

            $location->setLotType($lot_type)
                ->setIdLabel($this->em->find('App:LocationLabel', $lot_type))
                ->setIdTarif($this->em->find('App:Tarif', $lot_type))
                ->setAuth($this->em->find('App:Auth', mt_rand(2, 31) ));
            $manager->persist($location);
        }

        // Mobile-homes Camping
        for( $i=1; $i <= 20; $i++ )
        {
            $location = new LocationLot();
            $lot_type = mt_rand(1, 4);

            $location->setLotType($lot_type)
                ->setIdLabel($this->em->find('App:LocationLabel', $lot_type))
                ->setIdTarif($this->em->find('App:Tarif', $lot_type));
            $manager->persist($location);
        }

        // Caravanes
        for( $i=1; $i <= 10; $i++ )
        {
            $location = new LocationLot();
            $lot_type = mt_rand(5, 7);

            $location->setLotType($lot_type)
                ->setIdLabel($this->em->find('App:LocationLabel', $lot_type))
                ->setIdTarif($this->em->find('App:Tarif', $lot_type));
            $manager->persist($location);
        }

        // Emplacements
        for( $i=1; $i <= 30; $i++ )
        {
            $location = new LocationLot();
            $lot_type = mt_rand(8, 9);

            $location->setLotType($lot_type)
                ->setIdLabel($this->em->find('App:LocationLabel', $lot_type))
                ->setIdTarif($this->em->find('App:Tarif', $lot_type));
            $manager->persist($location);
        }
        $manager->flush();

        // Réservations
        for ($i=1; $i <= mt_rand(20, 40); $i++)
        {
            $user = new User();
            $user->setNom($faker->lastName)->setPrenom($faker->firstName())
                ->setEmail($faker->email);
            $manager->persist($user);

            $dateStartSeason= new DateTime("05-05-2021");
            $dateEndSeason = new DateTime("10-10-2021");

            // Génération des jours de d'arrivée
            $dateStart = new DateTime();
            $dateStart->setTimestamp( mt_rand(
                                        $dateStartSeason->getTimestamp(),
                                        $dateEndSeason->getTimestamp()) );

            $dateEnd = new DateTime();
            $dateEnd->setTimestamp( mt_rand(
                                        $dateStart->getTimestamp(),
                                        $dateEndSeason->getTimestamp()) );


            $reservation = new Reservation();
            $reservation->setIdUser($user)
                ->setIdLoc($this->em->find('App:LocationLot', rand(1, 90) ))
                ->setDateStart($dateStart)
                ->setDateEnd($dateEnd)
                ->setNbreAdulte(mt_rand(1,2))
                ->setNbreEnfants(mt_rand(0,2))
                ->setPoolAdultes(0)
                ->setPoolEnfants(0);

            /*
            if( $nbrEnfants > 0)
                $reservation->setPoolEnfants( mt_rand(0, $nbrJour) );
            */

            $manager->persist($reservation);
        }

        // Push dans BDD
        $manager->flush();
    }
}
