<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210705095550 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE auth (id INT AUTO_INCREMENT NOT NULL, id_user_id INT NOT NULL, password VARCHAR(255) NOT NULL, account_type INT NOT NULL, UNIQUE INDEX UNIQ_F8DEB05979F37AE5 (id_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE facture (id INT AUTO_INCREMENT NOT NULL, id_reservation_id INT NOT NULL, date_emission DATE NOT NULL, reduction TINYINT(1) NOT NULL, INDEX IDX_FE86641085542AE1 (id_reservation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location_label (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location_lot (id INT AUTO_INCREMENT NOT NULL, id_tarif_id INT NOT NULL, id_label_id INT NOT NULL, lot_type INT NOT NULL, INDEX IDX_6A53A28E65A7E6CC (id_tarif_id), INDEX IDX_6A53A28E6362C3AC (id_label_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reservation (id INT AUTO_INCREMENT NOT NULL, id_user_id INT NOT NULL, id_loc_id INT NOT NULL, date_start DATE NOT NULL, date_end DATE NOT NULL, nbre_adulte INT NOT NULL, nbre_enfants INT NOT NULL, pool_adultes INT NOT NULL, pool_enfants INT NOT NULL, INDEX IDX_42C8495579F37AE5 (id_user_id), INDEX IDX_42C8495543327F3C (id_loc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rservation (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tarif (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, prenom VARCHAR(50) NOT NULL, nom VARCHAR(50) NOT NULL, email VARCHAR(254) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE auth ADD CONSTRAINT FK_F8DEB05979F37AE5 FOREIGN KEY (id_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE facture ADD CONSTRAINT FK_FE86641085542AE1 FOREIGN KEY (id_reservation_id) REFERENCES reservation (id)');
        $this->addSql('ALTER TABLE location_lot ADD CONSTRAINT FK_6A53A28E65A7E6CC FOREIGN KEY (id_tarif_id) REFERENCES tarif (id)');
        $this->addSql('ALTER TABLE location_lot ADD CONSTRAINT FK_6A53A28E6362C3AC FOREIGN KEY (id_label_id) REFERENCES location_label (id)');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C8495579F37AE5 FOREIGN KEY (id_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C8495543327F3C FOREIGN KEY (id_loc_id) REFERENCES location_lot (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE location_lot DROP FOREIGN KEY FK_6A53A28E6362C3AC');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C8495543327F3C');
        $this->addSql('ALTER TABLE facture DROP FOREIGN KEY FK_FE86641085542AE1');
        $this->addSql('ALTER TABLE location_lot DROP FOREIGN KEY FK_6A53A28E65A7E6CC');
        $this->addSql('ALTER TABLE auth DROP FOREIGN KEY FK_F8DEB05979F37AE5');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C8495579F37AE5');
        $this->addSql('DROP TABLE auth');
        $this->addSql('DROP TABLE facture');
        $this->addSql('DROP TABLE location_label');
        $this->addSql('DROP TABLE location_lot');
        $this->addSql('DROP TABLE reservation');
        $this->addSql('DROP TABLE rservation');
        $this->addSql('DROP TABLE tarif');
        $this->addSql('DROP TABLE user');
    }
}
