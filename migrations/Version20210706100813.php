<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210706100813 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE facture DROP FOREIGN KEY FK_FE86641085542AE1');
        $this->addSql('DROP INDEX IDX_FE86641085542AE1 ON facture');
        $this->addSql('ALTER TABLE facture DROP id_reservation_id');
        $this->addSql('ALTER TABLE location_lot ADD auth_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE location_lot ADD CONSTRAINT FK_6A53A28E8082819C FOREIGN KEY (auth_id) REFERENCES auth (id)');
        $this->addSql('CREATE INDEX IDX_6A53A28E8082819C ON location_lot (auth_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE facture ADD id_reservation_id INT NOT NULL');
        $this->addSql('ALTER TABLE facture ADD CONSTRAINT FK_FE86641085542AE1 FOREIGN KEY (id_reservation_id) REFERENCES reservation (id)');
        $this->addSql('CREATE INDEX IDX_FE86641085542AE1 ON facture (id_reservation_id)');
        $this->addSql('ALTER TABLE location_lot DROP FOREIGN KEY FK_6A53A28E8082819C');
        $this->addSql('DROP INDEX IDX_6A53A28E8082819C ON location_lot');
        $this->addSql('ALTER TABLE location_lot DROP auth_id');
    }
}
